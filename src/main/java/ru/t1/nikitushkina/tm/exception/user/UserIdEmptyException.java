package ru.t1.nikitushkina.tm.exception.user;

public final class UserIdEmptyException extends AbstractUserException {

    public UserIdEmptyException() {
        super("Error! User Id is empty.");
    }

}
