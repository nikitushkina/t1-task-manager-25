package ru.t1.nikitushkina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.t1.nikitushkina.tm.api.service.IPropertyService;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String DESCRIPTION = "Display developer info.";

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final IPropertyService service = getPropertyService();
        System.out.println("[ABOUT]");
        System.out.println("name: " + service.getAuthorName());
        System.out.println("email: " + service.getAuthorEmail());
        System.out.println();
        System.out.println("[GIT]");
        System.out.println("branch: " + service.getGitBranch());
        System.out.println("commit id: " + service.getCommitId());
        System.out.println("commit time: " + service.getCommitTime());
        System.out.println("commit message: " + service.getCommitMsgFull());
        System.out.println("committer name: " + service.getCommitterName());
        System.out.println("committer email: " + service.getCommitterEmail());
    }

}
