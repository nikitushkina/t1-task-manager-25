package ru.t1.nikitushkina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.nikitushkina.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationConfig();

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    String getGitBranch();

    @NotNull
    String getCommitId();

    @NotNull
    String getCommitTime();

    @NotNull
    String getCommitMsgFull();

    @NotNull
    String getCommitterName();

    @NotNull
    String getCommitterEmail();

}
